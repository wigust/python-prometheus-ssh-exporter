(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "d96f47f012571cdd6dd67c513e496042db303ca7")))
