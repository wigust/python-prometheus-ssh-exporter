"""Connect to server over SSH and collect metrics."""

import argparse
import yaml
from prometheus_client import Counter, start_http_server
import jc.parsers.ifconfig
import logging
import os
import subprocess
import time

log = logging.getLogger("prometheus-ssh-exporter")
log.setLevel(logging.DEBUG)
logging.basicConfig()


def network_interface_properties_to_prometheus(
    device: dict, host: str, counter: Counter, interface_property: str
):
    if (device["name"], host) in counter._metrics:
        previous_value = counter._metrics[(device["name"], host)]._value._value
        if previous_value < device[interface_property]:
            log.debug(
                f"Previous value {previous_value} is bigger than current value {device[interface_property]} on device {device['name']}, skipping updating the value."
            )
            counter.labels(device=device["name"], target=host).inc(
                device[interface_property] - previous_value
            )
    else:
        if device[interface_property] is not None:
            counter.labels(device=device["name"], target=host).inc(
                device[interface_property]
            )
        else:
            log.debug(f"Device {device['name']} does not contain {interface_property}.")


def main():
    """Entry point."""
    running_user = os.getenv("USERNAME", "root")

    parser = argparse.ArgumentParser(description="Prometheus SSH exporter")
    parser.add_argument("-c", "--config", help="Config file", type=str)
    args = parser.parse_args()

    node_network_receive_bytes_total = Counter(
        "node_network_receive_bytes_total",
        "Network device statistic receive_bytes.",
        ["device", "target"],
    )

    node_network_transmit_bytes_total = Counter(
        "node_network_transmit_bytes_total",
        "Network device statistic transmit_bytes.",
        ["device", "target"],
    )

    node_network_receive_packets_total = Counter(
        "node_network_receive_packets_total",
        "Network device statistic receive_packets.",
        ["device", "target"],
    )

    node_network_transmit_packets_total = Counter(
        "node_network_transmit_packets_total",
        "Network device statistic transmit_packets.",
        ["device", "target"],
    )

    with open(args.config) as config_file:
        config = yaml.load(config_file, Loader=yaml.FullLoader)
        listen_addresses = config["listen"] if "listen" in config else "0.0.0.0:9101"
        interval = config["interval"] if "interval" in config else 10
        start_http_server(
            port=int(listen_addresses.split(":")[1]),
            addr=listen_addresses.split(":")[0],
        )
        while True:
            for host in config["hosts"].keys():
                user = (
                    config["hosts"][host]["username"]
                    if "username" in config["hosts"][host]
                    else running_user
                )
                port = (
                    config["hosts"][host]["port"]
                    if "port" in config["hosts"][host]
                    else 22
                )
                ifconfig = (
                    config["hosts"][host]["ifconfig"]
                    if "ifconfig" in config["hosts"][host]
                    else "ifconfig"
                )
                if "password" in config["hosts"][host]:
                    output = subprocess.getoutput(
                        f"sshpass -p{config['hosts'][host]['password']} ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 -F /dev/null {user}@{host} {ifconfig}"
                    )
                    devices = jc.parsers.ifconfig.parse(output)
                    log.debug(
                        {"host": host, "output": output}
                    ) if not devices else log.debug({"host": host, "devices": devices})
                else:
                    output = subprocess.getoutput(
                        f"ssh -i {config['hosts'][host]['ssh_private_key_file']} -p {port} -F /dev/null {user}@{host} {ifconfig}"
                    )
                    devices = jc.parsers.ifconfig.parse(output)
                    log.debug(
                        {"host": host, "output": output}
                    ) if not devices else log.debug({"host": host, "devices": devices})
                for device in devices:
                    network_interface_properties_to_prometheus(
                        device, host, node_network_receive_bytes_total, "rx_bytes"
                    )
                    network_interface_properties_to_prometheus(
                        device, host, node_network_transmit_bytes_total, "tx_bytes"
                    )
                    network_interface_properties_to_prometheus(
                        device, host, node_network_receive_packets_total, "rx_packets"
                    )
                    network_interface_properties_to_prometheus(
                        device, host, node_network_transmit_packets_total, "tx_packets"
                    )
                time.sleep(interval)


if __name__ == "__main__":
    main()
